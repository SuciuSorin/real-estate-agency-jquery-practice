

var properties = [

{
  type:"Purchase",
  rooms:2,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Aleea Castanilor - Berceni",
  surface:"110mp",
  floor:"1/10",
  code:"4444",
  price:"50.450 ",
  agent:"George Carlin"
},
{
  type:"Purchase",
  rooms:1,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Roma - Baba Novac",
  surface:"110mp",
  floor:"5/10",
  code:"1225",
  price:"80.450 ",
  agent:"Joe Rogan"
},
{
  type:"Purchase",
  rooms:2,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Paris - Baba Novac",
  surface:"90mp",
  floor:"10/10",
  code:"5645",
  price:"80.450 ",
  agent:"Seinfeld"
},
{
  type:"Purchase",
  rooms:3,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Paris - Baba Novac",
  surface:"90mp",
  floor:"2/10",
  code:"5645",
  price:"90.450 ",
  agent:"Bill Burr"
},
{
  type:"Purchase",
  rooms:2,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Paris - Baba Novac",
  surface:"90mp",
  floor:"2/10",
  code:"5645",
  price:"90.450 ",
  agent:"Bill Burr"
}
];





var filtered = $("#filtered");

function createDivs(prop) {
  var eachHouse = $("<div/>", {
      "class": "eachHouse"
  }).appendTo(filtered);

  // Create the div whitch holds the image
  $("<div/>",{
    "class": "filteredImage",
    "css"  : {"background-image": prop.image},     // "css"  : ("backgroundImage", "url("+properties.image+")")
  }).appendTo(eachHouse);

  // Create the div whitch holds the description of the property
  var houseDescription = $("<div/>", {
      "class": "houseDescription"
  }).appendTo(eachHouse);

  // Title of each property
  var propertyTitle = $("<h2/>", {
      "html": (prop.category + ", " + prop.rooms + " rooms" + ", " + prop.adress)
  }).appendTo(houseDescription);

  // Holds the small description - rooms, surface, floor, code
  var smallDescription = $("<div/>", {
      "class": "smallDescription"
  }).appendTo(houseDescription)

  // How many rooms
  var roomsSmall = $("<h4/>", {
      "html": ('<i class="fas fa-home" aria-hidden="true"></i>' + " " + prop.rooms +" rooms")
  }).appendTo(smallDescription)

  var surfaceSmall = $("<h4/>", {
      "html": ('<i class="fas fa-chart-pie"></i>' + " " + prop.surface)
  }).appendTo(smallDescription)

  var floorSmall = $("<h4/>", {
      "html": ('<i class="far fa-building"></i>' + " " + " Floor " + prop.floor)
  }).appendTo(smallDescription)

  var codeSmall = $("<h4/>", {
      "html": ("Code " +prop.code)
  }).appendTo(smallDescription)

  // Holds price, details button, agent
  var smallDescription2 = $("<div/>", {
      "class": "smallDescription2"
  }).appendTo(houseDescription)

  var price = $("<h4/>", {
      "class": "price",
      "html": (prop.price + ' <i class="fa fa-euro-sign" aria-hidden="true"></i>')
  }).appendTo(smallDescription2)

  // Gives margins to the div button
  var divButton = $("<div/>", {
      "class": "divButton"
  }).appendTo(smallDescription2)

  var priceButton = $("<button/>", {
      "class": "btn btn-primary detailsButton",
      "html": ("Details")
  }).appendTo(divButton)

  var agent = $("<div/>", {
      "class": "agent",
  }).appendTo(smallDescription2)

  var agentImg = $("<img/>", {
      "src": "./photos/avatar.png"
  }).appendTo(agent)

  var agentName = $("<a/>", {
      "href": "#",
      "html": (prop.agent)
  }).appendTo(agent)
}




$(document).ready(function(){
  $(".searchButton").click(function() {
    $("#filtered").html("");
    var type = $("#selectType option:selected").text();
    var rooms = $("#selectRooms option:selected").text();
    var petFriendly = $("#petFriendly option:selected").text();
    var category = $("#selectCategory option:selected").text();

    var propertiesFound = 0;

    for (var i = 0; i < properties.length; i++) {
           if ( properties[i].type     == type &&
                properties[i].rooms    == rooms &&
                properties[i].pet      == petFriendly &&
                properties[i].category == category){

                document.getElementById("hideContent").style.display = "none";
                document.getElementById("filtered").style.display = "block";

                  // console.log("avem");
                createDivs( properties[i] );
                propertiesFound++;
          }
      }
      if (propertiesFound == 0) {
          console.log("not found");
      }
  });
});
